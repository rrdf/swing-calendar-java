# Swing-calendar-java
A desktop app made using Java Swing to create calendar events

## To run
```
cd build/
java -jar swing-calendar-java-X.Y-SNAPSHOT.jar
```
where X.Y is the version number

## To build
To build the source, run
```
./mvnw clean package
```

## Screenshot
![screenshot](assets/screenshot.png)

## Credits
[SwingLink](https://github.com/dimo414/jgrep/blob/master/src/grep/SwingLink.java) by [dimo414](https://github.com/dimo414)
